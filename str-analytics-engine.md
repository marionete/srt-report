# SRT – Analytics Engine Design Document

|      Date      | Version | Status | Comments |
| :------------: | :-----: | :----: | :------: |
| 1st May   2020 |   0.1   | Draft  |          |
|                |         |        |          |

## Table of Contents

1. [Introduction](#Introduction)
    1. [Analytics Engine Goal](#Analytics-Engine-Goal)
    1. [Consultancy Objectives](#Consultancy-Objectives)
1. [Applicational Solution Architecture](#Applicational-Solution-Architecture)
    1. [Goals of the architecture](#Goals-of-the-architecture)
    1. [Data Ingestion and ETL](#Data-Ingestion-and-ETL)
    1. [Service layer and real-time applications](#Service-layer-and-real-time-applications)
1. [Application Resources](#Application-Resources)
    1. [RabbitMQ Resources](#RabbitMQ-Resources)
    1. [In-Memory](#In-Memory)
1. [Spark Applications](#Spark-Applications)
    1. [Spark Streaming](#Spark-Streaming)
        1. [How is it different from micro-batch](#How-is-it-different-from-micro-batch)
        1. [How “Continuous Processing” mode works](#How-“Continuous-Processing”-mode-works)
    1. [Estimate total memory](#Estimate-total-memory)
        1. [Explanation of formula](#Explanation-of-formula)
    1. [Tuning in Hadoop](#Tuning-in-Hadoop)
        1. [Example](#Example)
    1. [Calculate Level of Parallelism](#Calculate-Level-of-Parallelism)
        1. [Points of Control](#Points-of-Control)
1. [Client Remote Data Access](#Client-Remote-Data-Access)
    1. [WebHDFS](#WebHDFS)
1. [Migrating Data Between Clusters](#Migrating-Data-Between-Clusters)
    1. [Network Latency and Replication](#Network-Latency-and-Replication)
1. [Platform Stack](#Platform-Stack)
    1. [Apache Spark](#Apache-Spark)
    1. [Apache ZooKeeper](#Apache-ZooKeeper)
    1. [Apache Hadoop](#Apache-Hadoop)
        1. [Hadoop HDFS](#Hadoop-HDFS)
            1. [NameNode](#NameNode)
            1. [ZKFailoverController](#ZKFailoverController)
            1. [JournalNodes](#JournalNodes)
            1. [DataNode](#DataNode)
        1. [Hadoop YARN + MapReduce](#Hadoop-YARN-+-MapReduce)
            1. [ResourceManager](#ResourceManager)
            1. [TimeLine Server and History Server](#TimeLine-Server-and-History-Server)
            1. [NodeManager](#NodeManager)
        1. [Hadoop Layout](#Hadoop-Layout)
    1. [Apache Kafka](#Apache-Kafka)
        1. [Kafka Schema Registry](#Kafka-Schema-Registry)
        1. [Kafka Brokers](#Kafka-Brokers)
    1. [RabbitMQ](#RabbitMQ)
    1. [InlfuxDB](#InlfuxDB)
1. [Requirements](#Requirements)
    1. [System Requirements](#System-Requirements)
    1. [Hardware Specifications](#Hardware-Specifications)
    1. [Master Requirements](#Master-Requirements)
        1. [Hadoop Master Sizing](#Hadoop-Master-Sizing)
        1. [Kafka Master Sizing](#Kafka-Master-Sizing)
    1. [Master Hardware recommendations](#Master-Hardware-recommendations)
    1. [Slave Requirements](#Slave-Requirements)
        1. [Slave Storage Sizing](#Slave-Storage-Sizing)
        1. [Slave Computing Sizing](#Slave-Computing-Sizing)
    1. [Slave Hardware recommendations](#Slave-Hardware-recommendations)

## Introduction

Marionete has been asked to provide consultancy support in the form of a review of an Analytics Engine to be built by SRT. The review looked at the technology solution in place and the wider ecosystem in which it sat and operated.

### Analytics Engine Goal

The analytics engine (AE) will be used to produce results from advanced analytics. These may include:

- generating high-resolution density maps;
- Creating vessel traffic maps;
- identifying suspicious vessels through pattern analysis (gps spoofing, dark transponders);
- producing oceanic models;

The AE is a specialised architecture that will allow us to perform geospatial analyses on big data as well as run machine learning queries.

It will have the same data as that GeoVS uses and it should be in real-time; this is so if the AE raises an alert on a vessel, the AIS positional data will already be available in the hub.

> If the data was not the same, the analytics engine might produce an alert that the hub does not know about.

The models and analysis results will be accessed by an API.

Non-functional requirements:

- A high-resolution density map must be produced within ~30 minutes;
- The solution must be able to cope with five years worth of data;

### Consultancy Objectives

The **Key Deliverables** for phase one are as follows:

- Verify the design of the [Apache Spark](https://spark.apache.org) Engine;
- Hardware options for Apache Spark Engine;
- Discuss and advise on best practice about managing the cluster and running spark workloads operationally with Apache Spark Engine on-premises in mind;
- Communicate and provide main design recommendations from [Marionete](http://marionete.co.uk) concerning the Apache Spark Engine;
- Provide hardware specification and a kit list for Apache Spark Engine;
- Provide Recommendations for tooling in relation to the Apache Spark Engine;

## Applicational Solution Architecture

The following sections discuss a proposal of an applicational solution to host the Analytics Engine, covering:

- Data Ingestion;
- ETL;
- Storage;
- Workflow Orchestration;
- Client Interfaces;

### Goals of the architecture

The main goals of the architecture are:

- Decoupled event ingestion layer that can work with multiple independent sources and receiving systems;
- Ability to process streams of events coming from multiple input systems;
- Ability to react to outcomes of processing logic;

### Data Ingestion and ETL

This section describes the solution from data ingestion until the initial ETL.

- Data Ingestion and Collection Layer: Kafka;
- Data processing layer: Spark, Kafka Streams;

Kafka allows for durable and scalable ingestion of streams of events coming from many producers to many consumers.

Spark can process large amounts of data, including real-time and near-real-time streams of events.

Using this stack, the solution can easily be used to:

- Receive data from a variety of sources;
- Perform specific computation and analysis on data on the fly;
- Perform an action as a result;

The following diagram illustrates the data ingestion layer.

![srt-high-level-1](img/srt-high-level-1.jpeg)

The data is made available via [Kafka](https://kafka.apache.org) topics (`1`), we recommend the use of one topic per data schema; this dramatically simplifies the logic in the consumer side, by consuming messages uniformly.

Additionally, the use of a [schema register](https://docs.confluent.io/current/schema-registry/index.html) is also recommended (`2`) since it provides compatibility checks to ensure that the contract between producers and consumers is not broken; this is especially important in Kafka because producers and consumers are decoupled; it provides a serving layer for metadata, supporting [Avro](https://avro.apache.org), [JSON](https://www.json.org/json-en.html) and [Protobuf](https://developers.google.com/protocol-buffers) schemas.

A schema registry lives outside of and separately from the Kafka brokers. Your producers and consumers still talk to Kafka to publish and read data (messages) to topics. Concurrently, they can also talk to Schema Registry to send and retrieve schemas that describe the data models for the messages.

We have one dedicated consumer to commit the raw data to storage (`3`). We call this area the *Raw Zone* as it contains the raw data exactly in its original format. We recommend a partitioning scheme by source and date.

A Spark Streaming application (`4`) consumes the data from the upstream Kafka topics; The data is not written directly to storage but instead is pushed again to a Kafka topic that holds the parsed/structure version of the data. We should have one stream per topic/data type. These streams will parse the data (we expect the data to be available in highly custom formats), perform validations and basic transformations to structure the data in a tabular form. Spark operates via micro-batch processing. With micro-batch processing, the engine periodically checks the streaming source and runs a batch query on new data that has arrived since the last batch ended. This way latencies happen to be around 100s of milliseconds (see [Spark Streaming](https://spark.apache.org/streaming/)).

In case we require sub-second latency, a Kafka Stream (`5`) can be used instead of a Spark Streaming application. From our experience, these are harder to maintain and evolve and offer a less powerful API for data manipulation. The advantage of Kafka Streams is due to the fact that each event is processed as soon as it is available, whereas Spark Streaming works in micro-batches. The choice of a streaming platform depends on latency guarantees, community adoption and interoperability with the current ecosystem.

Once the data is again in Kafka it can be consumed in parallel by multiple consumers. Having one consumer dedicated to committing the data to disk (`6`) allows other applications to consume the data without any lag. We call this area *Structured Zone*, since the data at this point is already parsed and structured.

Additionally, we recommend the use of a columnar physical format to store the data at this stage, e.g.: [ORC](https://orc.apache.org) or [Parquet](https://parquet.apache.org); This is important since this data will serve various analytical workloads. Spark knows how to leverage these physical formats, e.g.: predicate pushdown.

### Service layer and real-time applications

Clients interface with the platform via a service layer, which can be used to access and manipulate data dynamically or to trigger the execution of long-running specialised reports.

The following components are proposed:

- Message Broker: [RabbitMQ](https://www.rabbitmq.com);
- In-memory DB: [InfluxDB](https://www.influxdata.com/products/influxdb-overview/);
- Data processing layer: Spark;

The following diagram illustrates the client's interfaces and real-time applications.

![srt-high-level-2](img/srt-high-level-2.jpeg)

> Note that *Topic A2* (`7`) at the top of the diagram refers to the same topic illustrated in the previous section.

*Topic A2* (`7`)  holds the stream of post-ETL data; this stream can be used to for streaming analytics (`8`).  Using Spark Streaming, the streaming computation the same way that we would express a batch computation on static data. The Spark SQL engine will take care of running it incrementally and continuously and updating the final result as streaming data continues to arrive.

Spark Streaming applications operate within a *window* of data, which is useful for use cases that require some context/memory to make a decision and cannot operate independently for each record of data.

A model repository (`9`) holds the pre-trained models embedded by the streaming applications.

An additional consumer (`10`) can also commit data directly to an in-memory database; this will allow users to query newly arrived data in the platform.

The streaming applications can push alerts/notifications of interest back to Kafka (`11`). An example of this would be a streaming application that is detecting illegal fishing activity while generating an alert for each detected instance.

The platform exposes two interfaces:

- a SQL Interface (`12`);
- services to generate Reports (`13`);

The SQL interface is done via a connector, e.g.: JDBC

- Spark SQL to query historical data in both the Structured and Consume zone;
- InfluxDB for real-time SQL queries;

To generate a report supported by the platform, a user submits the request via a REST interface that will handle the request (`13`). The message should specify the input parameters supported by the report; The REST interface returns an ID to the user that identifies the report (`14`).

Internally, the REST service handles the request in an asynchronous fashion via a message broker (`15`). A consumer (`17`) is responsible for orchestrating the execution of a batch Spark application (`16`).

> We recommend the use of a workflow management tool, e.g.: Apache Airflow, to decouple the consumer of the report queue with  the details of orchestrating a Spark application.

Once the result is available, it is posted back to the client-side with the correct ID (`18`).

## Application Resources

This section discusses the requirements in terms of CPU and RAM for the different technologies proposed in the architecture. Assumptions are made in relation to the data size and the number of requests. These can be adapted once more accurate information is available.

|                        | RAM        | CPU    |
|------------------------|-----------:| ------:|
| Spark - Heavier Job`¹` | 100 GB     | 16     |
| Spark - SQL Query`²`   | 30 GB      | 6      |
| Spark RTS`³`           | 10 GB      | 2      |
| RabbitMQ               | 8 GB       | 4      |
| In-Memory              | 20 GB      | 4      |
|                        |            |        |

`¹` We assume the most computationally expensive batch job needs to process `30-40 GB` of uncompressed data and we want to run the job with the full dataset in-memory;

`²` We assume a requirement where users might be querying `10-15 GB` of data in the Structured Zone in parallel with a long-running batch job.

`³` We assume **a single** Spark Streaming Job handling one analytical use case (post ETL).

> We will discuss the number over the Spark in the next chapter.

### RabbitMQ Resources

By default, when the RabbitMQ server uses above `40%` of the available RAM, it raises a memory alarm and blocks all connections that are publishing messages.

Assume `10K` messages/second, occasionally growing to `100K`.

`400 bytes/message`

There's also message metadata from `AMQP` (e.g message properties), and the queue
maintains additional message metadata, e.g., internal message ID, place in the queue, etc.; so typically we assume 1 KB per message in addition to the message body, when the message is fully RAM-resident.

RAM required = `10M record * (400 + 1000) bytes/record =  1.5 GB`

So `4 cores`, `4-8 GB of RAM` should be OK.

### In-Memory

Example: Using InfluxDB.

A single node is sufficient whenever we don’t require:

- more than 750,000 field writes per second
- more than 100 moderate queries per second

## Spark Applications

The data processing layer relies on Spark for both batch and real-time processing.

This section discusses more technical aspects of a Spark application and provides recommendations on how to make the best use of computational resources.

### Spark Streaming

[Spark Streaming](https://spark.apache.org/streaming/) is an extension of the core Spark API that enables scalable, high-throughput, fault-tolerant stream processing of live data streams.

Internally, it works as follows. Spark Streaming receives live input data streams and divides the data into batches, which are then processed by the Spark engine to generate the final stream of results in batches.

#### How is it different from micro-batch

With micro-batch processing, Spark streaming engine periodically checks the streaming source, and runs a batch query on new data that has arrived since the last batch ended. This way latencies happen to be around 100s of milliseconds.

![Spark-1](img/spark-1.png)

Spark driver checkpoints the progress by saving record offsets to a write-ahead-log, which may be then used to restart the query.

Recording of offsets for next batch of records is happening before the batch started processing.
This way, some records have to wait until the end of the current micro-batch to be processed, and this takes time.

#### How “Continuous Processing” mode works

Spark launches a number of long-running tasks. They constantly read, process and write data. Events are processed as soon as they’re available at the source.

![Spark-2](img/spark-2.png)

We are capable of achieving end-to-end latency of just a few milliseconds because events are processed and written to sink as soon as they are available in the source, without waiting for other records.

Also, checkpointing is fully asynchronous and uses the [Chandy-Lamport](https://en.wikipedia.org/wiki/Chandy%E2%80%93Lamport_algorithm) algorithm, so nothing interrupts tasks and Spark is able to provide consistent millisecond-level latencies.

### Estimate total memory

An approximated calculation for the size of a dataset is

```math
number Of Megabytes = M = (N*V*W) / 1024^2
```

where

```math
N  =  number of records    V  =  number of variables    W  =  average width in bytes of a variable
```

In approximating **W**, remember:

| Type of variable                                | Width          |
|-------------------------------------------------|---------------:|
| Integers `(−127 ≤ x ≤ 100)`                     | 1              |
| Integers `(32,767 ≤ x ≤ 32,740)`                | 2              |
| Integers `(-2,147,483,647 ≤ x ≤ 2,147,483,620)` | 4              |
| Floats single precision                         | 4              |
| Floats double precision                         | 8              |
| Strings                                         | maximum length |
|                                                 |                |

Say that you have a `20,000-observation` dataset. That dataset contains

```math
    1  string identifier of length 20                     20
    10  small integers (1 byte each)                      10
    4  standard integers (2 bytes each)                    8
    5  floating-point numbers (4 bytes each)              20
    --------------------------------------------------------
    20  variables total                                   58
```

Thus the average width of a variable is

```math
W = 58/20 = 2.9  bytes
```

The size of your dataset is

```math
M = 20000 x 20 x 2.9 /1024^2 = 1.13 megabytes
```

This result slightly understates the size of the dataset because we have not included any variable labels, value labels, or notes that you might add to the data. That does not amount to much. For instance, imagine that you added variable labels to all 20 variables and that the average length of the text of the labels was 22 characters.

That would amount to a total of `20*22=440 bytes or 440/10242=.00042 megabytes`.

#### Explanation of formula

```math
M = 20000 x 20 x 2.9 / 1024^2 = 1.13 megabytes
```

`N*V*W` is, of course, the total size of the data. The `1,0242` in the denominator rescales the results to megabytes.

Yes, the result is divided by `1,0242` even though `1,0002 = a million`. Computer memory comes in binary increments. Although we think of k as standing for kilo, in the computer business, k is really a “binary” thousand, `210 = 1,024`. A megabyte is a binary million—a binary k squared:

```math
1 MB = 1024 KB = 1024 x 1024 = 1,048,576 bytes
```

 > **NOTE**: Due to Java Object overhead, the data footprint **will expand** when the data is deserialized from disk into Java objects in memory; so it **will usually be larger in memory**.

### Tuning in Hadoop

When allocating resources to a spark application, we should keep some things in mind:

- **Hadoop/Yarn/OS Deamons**. When we run spark application using a cluster manager like Yarn, there’ll be several daemons that’ll run in the background like NameNode, Secondary NameNode, DataNode, JobTracker and TaskTracker. So, while specifying num-executors, we need to make sure that we leave aside enough cores (~1 core per node) for these daemons to run smoothly.
- **Yarn ApplicationMaster (AM)**. ApplicationMaster is responsible for negotiating resources from the ResourceManager and working with the NodeManagers to execute and monitor the containers and their resource consumption. If we are running spark on yarn, then we need to budget in the resources that AM would need (~1024MB and 1 Executor).
- **HDFS Throughput**: HDFS client has trouble with tons of concurrent threads. It was observed that HDFS achieves full write throughput with ~5 tasks per executor. So it’s good to keep the number of cores per executor below that number.

The memory resources allocated for a Spark application should be greater than that necessary to cache, shuffle data structures used for grouping, aggregations, and joins.

There are three considerations in tuning memory usage: the amount of memory used by your objects, the cost of accessing those objects, and the overhead of garbage collection (GC).

Also note that:

```mash
Full memory requested to yarn per executor = spark-executor-memory + spark.yarn.executor.memoryOverhead

spark.yarn.executor.memoryOverhead = Max(384MB, 7% of spark.executor-memory)
```

So, if we request 20GB per executor, AM will actually get 20GB + memoryOverhead = 20 + 7% of 20GB = ~23GB memory for us.

Additional considerations:

- Running executors with too much memory often result in excessive garbage collection delays.
- Running tiny executors (with a single core and just enough memory needed to run a single task, for example) throws away the benefits that come from running multiple tasks in a single JVM.
- Shared/cached variables like broadcast variables and accumulators are be replicated in each executor.
- Each executor has a JVM.
- An executor with more than one core can run multiple tasks in the same JVM.

#### Example

Suppose we have a 10 node cluster with the following specs:

```math
10 Nodes 16 cores per Node 64GB RAM per Node
```

- Based on the recommendations mentioned above, Let's assign 5 core per executors:
  `--executor-cores = 5` (for good HDFS throughput)
- Leave 1 core per node for Hadoop/Yarn daemons
  Num cores available per node = `16-1 = 15`
- Total available of cores in cluster = `15 x 10 = 150`
- Number of available executors = (total cores/num-cores-per-executor) = `150/5 = 30`
- Leaving 1 executor for ApplicationManager
  `--num-executors = 29`
- Number of executors per node = `30/10 = 3`
- Memory per executor = `64GB/3 = 21GB`
- Counting off heap overhead = `7% of 21GB = 3GB`
   So, actual `--executor-memory = 21 - 3 = 18GB`

**Recommended configuration:** 29 executors, 18GB memory each and 5 cores each.

### Calculate Level of Parallelism

It is crucial to ensure that Spark application runs with the right level of parallelism. This is useful to ensure that we are leveraging the cluster's resources and also to prevent OOM errors.

By default, spark creates one partition for each block of the file in HDFS (64 / 128 MB by default)

Generically the size of the dataset can be given by:

`M = N * V * S`

Where N is the number of records, V the number of variables (columns) and S the average width in bytes of a variable. The later should take into account the types of each variable and factor that into the average calculation.

Let's assume an average record size of 800 bytes.

`S ~= 800 bytes`

The number of worker units that we have to process tasks in parallel is given by:

`Units = Number of Executors x Number Cores / Executor`

A heuristic is to point each partition to have around 128 MB of data. This is due to the default HDFS block size.

So we need to dynamically calculate a parallelism factor m, which in turn should depend on the number of units available and the number of records to process.

`m = (N * S) / (128 MB * Units)`

#### Points of Control

We recommend defining a constant `DESIRED_PARTITION_SIZE` (default 128 MB) to control the average size of the partition to be processed by each task. `AVG_SIZE_RECORD` should be an estimate for the average record size. We recommend estimating this value periodically using a sample of data to ensure that the profile of the data did not suffer deviations from this value.

If the number of calculated partitions is shown below; the available units of work (`executors x cores`), e.g. a weekend day with where only `500 K` records were reported, we chose to trade off efficiency over a good block size, i.e. even though we might have N units of work available, we set the level of parallelism to `P` where `P < N`.

## Client Remote Data Access

As long as an application needs to access data stored in HDFS from inside a cluster or another machine on the network, it can use a high-performance native protocol or the native Java API. However, for situations where clients with external application need to access or manage files in the HDFS, we need a solution that can easily work over HTTP.

For these kinds of requirements, the [WebHDFS](https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-hdfs/WebHDFS.html) protocol is available.

### WebHDFS

WebHDFS, is based on an industry-standard RESTful mechanism that does not require Java binding. It works with operations such as reading files, writing to files, making directories, changing permissions, and renaming. It defines a public HTTP REST API, which permits clients to access HDFS over the Web. Clients can use common tools such as curl/wget to access the HDFS.

WebHDFS provides web services access to data stored in HDFS. At the same time, it retains the security the native Hadoop protocol offers and uses parallelism, for better throughput.

To enable WebHDFS (REST API) in the name node and data nodes, the value of `dfs.webhdfs.enabled` configuration property must be set to `true` in `hdfs-site.xml` configuration file.

- **Wire Compatibility**: the REST API will be maintained for wire compatibility. Thus, WebHDFS clients can talk to clusters with different Hadoop versions.
- **Secure Authentication**: The core Hadoop uses Kerberos and Hadoop delegation tokens for security. WebHDFS also uses Kerberos (SPNEGO) and Hadoop delegation tokens for authentication.
- **Data Locality**: The file read and file write calls are redirected to the corresponding datanodes. It uses the full bandwidth of the Hadoop cluster for streaming data.

## Migrating Data Between Clusters

This section discusses how to transfer data between two hadoop clusters. Hadoop distributions have available the [distcp](https://hadoop.apache.org/docs/stable/hadoop-distcp/DistCp.html) command, which is used to transfer the data between clusters.

One of the main use cases of `distcp` is to sync the data between production cluster and backup/DR cluster. Or when moving data between development, research, and production cluster environments.  Hadoop Distributed Copy (`distcp`) is a tool for efficiently copying large amounts of data within or in between clusters.

It uses MapReduce to effect its distribution, error handling and recovery, and reporting. It expands a list of files and directories into input to map tasks, each of which will copy a partition of the files specified in the source list.

The most common use of DistCp is an inter-cluster copy:

```code
hadoop distcp hdfs://nn1:8020/source hdfs://nn2:8020/destination
```

When copying data from one cluster to another we only touch the raw data (without the replication factor). HDFS handles the replication of blocks internally.

The source and destination cluster must use the same RPC protocol. Typically, this means that the source and destination cluster should have the same version of Hadoop installed.

### Network Latency and Replication

High latency among clusters can cause replication jobs to run more slowly, but does not cause them to fail. For best performance, latency between the source cluster NameNode and the destination cluster NameNode should be less than 80 milliseconds. (Test latency using the Linux ping command.)

> **Note:** [Cloudera has successfully tested replications with latency of up to 360 milliseconds.](https://docs.cloudera.com/cloudera-manager/7.0.3/replication-manager/topics/rm-dc-network-latency-and-replication.html) As latency increases, replication performance degrades.

The throughput of the replication job depends on the absolute read and write throughput of the source and destination clusters.

## Platform Stack

Looking into the AE design the stack will be made up of:

- [Apache Spark](#Apache-Spark)
- [Apache ZooKeeper](#Apache-ZooKeeper)
- [Apache Hadoop](#Apache-Hadoop)
- [Apache Kafka](#Apache-Kafka)
- [RabbitMQ](#RabbitMQ)
- [InfluxDB](#InfluxDB)

### Apache Spark

[Apache Spark](https://spark.apache.org) is a unified analytics engine and a set of libraries for parallel data processing on computer clusters.

Spark limits its scope to a computing engine. Spark handles loading data from external storage systems and performing computation on it, not permanent storage as the end itself.

Spark applications run as independent sets of processes on a cluster, the system currently supports several cluster managers:

- [Standalone](https://spark.apache.org/docs/latest/spark-standalone.html), a simple cluster manager included with Spark that makes it easy to set up a cluster.
- [Apache Mesos](https://spark.apache.org/docs/latest/running-on-mesos.html)
- [Hadoop YARN](https://spark.apache.org/docs/latest/running-on-yarn.html), the resource manager in Hadoop 2.
- [Kubernetes](https://spark.apache.org/docs/latest/running-on-kubernetes.html), an open-source system for automating deployment, scaling, and management of containerized applications.

For the current project, the cluster manager in consideration is YARN.

The only component to be running at all time for the [Apache Spark](https://spark.apache.org) will be the [Spark History Server](https://spark.apache.org/docs/latest/monitoring.html) that creates a web interface, listing incomplete and completed applications and attempts.

Usually the jobs are configured to log events in the same shared, writable directory inside HDFS.

There are no special recommendations for this component and can be co-locate in a host with other daemons.

| Service   | Component      | Type   | # Cores | Heap Size | Disk      |
|-----------|----------------|--------|--------:|----------:|:---------:|
| Spark     | History Server | Master |       1 |      2 GB | -         |
|           |                |        |         |           |           |

### Apache ZooKeeper

[Apache ZooKeeper](https://zookeeper.apache.org) is a centralized service for maintaining configuration information, naming, providing distributed synchronization, and providing group services, used as a backend by distributed applications, in our case for the [Apache Hadoop](https://hadoop.apache.org) and [Apache Kafka](https://kafka.apache.org).

![ZooKeeper Logo](./img/zookeeper.png)

For a production environment we should deploy in a cluster known as an ensemble. A minimum of three servers are required for a fault tolerant clustered setup, and it is strongly recommended that you have an odd number of servers, because Zookeeper requires a majority.

> In ZooKeeper, quorum is the minimum number of servers that have to be running and available in order for ZooKeeper to work.

ZooKeeper runs in Java, release 1.8 or greater. Any major Linux distribution should be consider when running in production.

Although ZooKeeper does not heavily consume CPU resources.You should consider providing a dedicated CPU core to the ZooKeeper Server.

ZooKeeper is not a memory intensive application but 4 GB should be reserved and a starting heap size of 1 GB to be considered.

> At Yahoo!, ZooKeeper is usually deployed on dedicated RHEL boxes, with dual-core processors, 2GB of RAM, and 80GB IDE hard drives. [¹](https://zookeeper.apache.org/doc/r3.5.6/zookeeperAdmin.html#sc_systemReq).

Disk performance is vital to maintaining a healthy ZooKeeper cluster as The ZooKeeper server continually saves `znode` snapshot files and, optionally, transactional logs in a Data Directory to enable you to recover data.

Although ZooKeeper is highly reliable because a persistent copy is replicated on each server, recovering from backups may be necessary if a catastrophic failure or user error occurs. It's a good idea to back up the ZooKeeper Data Directory periodically.

At least one dedicated 100 GB should be considered. [Cloudera](https://www.cloudera.com) recommends HDD drivers [²](https://docs.cloudera.com/documentation/enterprise/6/release-notes/topics/rg_hardware_requirements.html#concept_enb_1nb_jbb) but [Confluent](https://www.confluent.io) recommends SDD[³](https://docs.confluent.io/current/zookeeper/deployment.html#disks).

> For full performance optimization we would consider having two disks, one for the `dataDir` and another for the `dataLogDir` to help avoid competition between logging and snapshots, but at this stage treat it as optional and future reference.

Like mentioned, three servers is enough for a production install, but for maximum reliability during maintenance, we should consider five.

| Ensemble | Quorum | Node Failures tolerate  |
|---------:|-------:|------------------------:|
|        1 |      1 |                       0 |
|        2 |      2 |                       0 |
|        3 |      2 |                       1 |
|        4 |      3 |                       1 |
|        5 |      3 |                       2 |
|          |        |                         |

With three servers, if we perform maintenance on one of them, we are vulnerable to a failure on one of the others. If we have five we can perform maintenance on one and in the event of a failure on another we are still within the quorum.

Now, keep in mind that we have two racks available. Meaning that one of the rack will always constitute a majority, and if something happens at a rack level ZooKeeper would stop.

> We need to have automated actions ready to deal with this scenario: reconfigure ensemble, add/remove nodes, reconfigure clients that use the ZooKeeper. Having other nodes ready to take over the role is also a good choice.

Due to the small footprint of the ZooKeeper, we don't need to have dedicated servers, we can co-locate in a host with other daemons. We can run a 5-node ensemble using the masters available for Hadoop (2 masters) and Kafka (3 masters).

Resources per instance:

| Service   | Component        | Type   | # Cores | Heap Size | Disk      |
|-----------|------------------|--------|--------:|----------:|----------:|
| ZooKeeper | ZooKeeper Server | Master |       1 |      1 GB | 1x 100 GB |
|           |                  |        |         |           |           |

### Apache Hadoop

The [Apache Hadoop](https://hadoop.apache.org) project, is a framework that allows for the distributed processing of large data sets across clusters of computers, , includes these modules:

- **Hadoop Common** - The common utilities that support the other Hadoop modules.
- **Hadoop Distributed File System** (HDFS) - A distributed file system that provides high-throughput access to application data.
- **Hadoop YARN** - A framework for job scheduling and cluster resource management.
- **Hadoop MapReduce** - A YARN-based system for parallel processing of large data sets.
- **Hadoop Ozone** - An object store for Hadoop.

> [Hadoop Ozone](https://hadoop.apache.org/ozone/) is still in beta and not in consideration for this project.

Apache Hadoop 3.x now supports only Java 8, from 2.7.x to 2.x support Java 7 and 8. Any major Linux distribution should be consider when running in production.

#### Hadoop HDFS

The Hadoop Distributed File System ([HDFS](https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-hdfs/HdfsDesign.html)) is a distributed file system designed to run on commodity hardware.

We are presenting an HDFS High Availability (HA) using the [Quorum Journal Manager](https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-hdfs/HDFSHighAvailabilityWithQJM.html) architecture. For automatic fail-over we need a [ZooKeeper Ensemble](https://zookeeper.apache.org) already discussed and not represented in the diagram.

![HDFS Arch](./img/HDFS-Arch.png)

Although most of the daemons are not CPU intensive, NameNodes (NNs) are very 'chatty' and a minimum of 4 dedicated cores is recommended only for the NN.

By default, all HDFS daemons will run with 1 GB of heap size. For the DataNodes (DNs) we should consider starting with a 4 GB heap size.

##### NameNode

An HDFS cluster consists of a single Active NameNode (aNN), a master server that manages the file system namespace and regulates access to files by clients andm in this architecture, a Standby NameNode (SbNN)  responsible for checkpointing.

The NN stores modifications to the file system as a log appended to a native file system file, `edits`.

![edit log](./img/checkpointing1.png)

When a NN starts up, it reads HDFS state from an image file, `fsimage`, and then applies edits from the edits log file.

It then writes new HDFS state to the `fsimage` and starts normal operation with an empty edits file.

Since NN merges fsimage and edits files only during start up, the edits log file could get very large over time on a busy cluster. Another side effect of a larger edits file is that next restart of NN takes longer.

The Standby NameNode (SbNN) merges the `fsimage` and the edits log files periodically and keeps edits log size within a limit.

![edit log](./img/checkpointing2.png)

Usually, the recommendation is to have, at least, 2 dedicated disk for the NN metadata. Consider to have, at least, one dedicated disk to store all hadoop related data.

> HDFS metadata backups can be used to restore a NN when both NNs roles have failed. In addition, backing up HDFS metadata before a major upgrade is also recommended.

The NN is a core component of HDFS, hence we are applying HA and automatic fail-over. The SbNN should have the same specs as the aNN.

| Service   | Component | Type   | # Cores | Heap Size | Disk      |
|-----------|-----------|--------|--------:|----------:|----------:|
| HDFS      | NN        | Master |       4 |      1 GB | 1x 500 GB |
|           |           |        |         |           |           |

> The default recommendation is 1 GB of NN heap space per million blocks. In practice, your heap requirements will likely be less than this conservative estimate.

Having two racks we should put one NN in each, if we loose one HDFS will still be serving, the only issue on having the SbNN down is that checkpointing is suspended.

##### ZKFailoverController

The ZKFailoverController (ZKFC) is a ZooKeeper client which also monitors and manages the state of the NN. Although a slave, it needs to be co-located with the NNs.

| Service   | Component      | Type   | # Cores | Heap Size | Disk      |
|-----------|----------------|--------|--------:|----------:|:---------:|
| HDFS      | ZKCF           | Slave  |       1 |      1 GB | -         |
|           |                |        |         |           |           |

##### JournalNodes

JournalNodes (JNs) are used to synchronize active and standby NNs. The aNN writes to each JN with changes to HDFS namespace metadata.

During failover, the sNN applies all edits from the JNs before promoting itself to the active state.

The JN daemon is relatively lightweight:

| Service   | Component      | Type   | # Cores | Heap Size | Disk      |
|-----------|----------------|--------|---------|-----------|-----------|
| HDFS      | JN             | Slave  |       1 |      1 GB | -         |
|           |                |        |         |           |           |

There must be at least 3 JNs daemons, since edit log modifications must be written to a majority of JNs.

> Again, because we have two racks to be used we should take into consideration the possibility to loose the majority and have automated actions to recreate in a spare node.

##### DataNode

DataNodes (DNs) store data in a Hadoop cluster and is the name of the daemon that manages the data.

File data is replicated on multiple DNs for reliability and so that localized computation can be executed near the data.

Within a cluster, DNs should be uniform. If they are not uniform, issues can occur.

The daemon itself should be sized like:

| Service   | Component | Type   | # Cores | Heap Size |
|-----------|-----------|--------|---------|----------:|
| HDFS      | DN        |  Slave |       1 |      4 GB |
|           |           |        |         |           |

> The actual size for HDFS will be discuss on another topic under Hardware Specification.

#### Hadoop YARN + MapReduce

[Apache Hadoop Yarn](https://hadoop.apache.org/docs/stable/hadoop-yarn/hadoop-yarn-site/YARN.html) (Yet Another Resource Negotiator) is Hadoop’s cluster resource management system.

[MapReduce](https://hadoop.apache.org/docs/stable/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html) is a programming model for data processing.

The diagram bellow shows the implementation of YARN/MR with [ResourceManager High Availability](https://hadoop.apache.org/docs/stable/hadoop-yarn/hadoop-yarn-site/ResourceManagerHA.html) in place.

![HDFS Arch](./img/YARN-Arch.png)

> No additional daemons are required for HA, opposed to what happens in HDFS. The RM just needs to have a [ZooKeeper Ensemble](https://zookeeper.apache.org), which was already discussed and not represented in the diagram.

Most of the daemons are not CPU intensive, consider reserve 1 core per daemon. We can start to use all daemons with the 1 GB default, with the exception of the RM, which we should consider starting with 3 GB.

##### ResourceManager

The ResourceManager (RM) is the ultimate authority that arbitrates resources among all the applications in the system, having two main components: Scheduler and ApplicationsManager.

Hence, running in HA is a must and [ResourceManager Restart](https://hadoop.apache.org/docs/stable/hadoop-yarn/hadoop-yarn-site/ResourceManagerRestart.html) has to be in place in order to keep functioning across restarts and also makes RM down-time invisible to end-users.

| Service   | Component | Type   | # Cores | Heap Size | Disk      |
|-----------|-----------|--------|--------:|----------:|:---------:|
| YARN      | RM        | Master |       1 |      3 GB |     -     |
|           |           |        |         |           |           |

##### TimeLine Server and History Server

Both the YARN TimeLine Server ([ATS](https://hadoop.apache.org/docs/stable/hadoop-yarn/hadoop-yarn-site/TimelineServer.html)) and the MR History Server ([JHS](https://hadoop.apache.org/docs/stable/hadoop-mapreduce-client/hadoop-mapreduce-client-hs/HistoryServerRest.html)) are SPOF.

| Service   | Component | Type   | # Cores | Heap Size | Disk      |
|-----------|-----------|--------|---------|-----------|:---------:|
| YARN      | ATS       | Master |       1 |      1 GB |     -     |
| RM        | JHS       | Master |       1 |      1 GB |     -     |
|           |           |        |         |           |           |

> We should leave a margin for growth in terms of Heap Size and have mechanisms in-place to manually failover to the other master if required.

##### NodeManager

The NodeManager (NM) is the per-machine framework agent who is responsible for containers, monitoring their resource usage (cpu, memory, disk, network) and reporting the same to the RM.

For the NM we should pay attentions to the Local and Log directories and put dedicated disks on them. For the expected usage, at this time, using only one dedicated disk will be enough.

| Service   | Component | Type   | # Cores | Heap Size | Disk      |
|-----------|-----------|--------|---------|-----------|----------:|
| YARN      | NM        |  Slave |       1 |      1 GB | 1x 500 GB |
|           |           |        |         |           |           |

> In terms of actual computation needed for the workloads, we will address them over Hardware Specification.

#### Hadoop Layout

Looking at all the components required in the Hadoop Ecosystem, we have:

| Service   | Component | Type   | # Cores | Heap Size | Disk      |
|-----------|-----------|--------|--------:|----------:|:----------|
| ZooKeeper | ZK        | Master |       1 |      1 GB | 1x 100 GB |
| HDFS      | NN        | Master |       4 |      1 GB | 1x 500 GB |
| HDFS      | ZKFC      |  Slave |       1 |      1 GB |     -     |
| HDFS      | JN        |  Slave |       1 |      1 GB |     -     |
| HDFS      | DN        |  Slave |       1 |      4 GB |     -     |
| YARN      | RM        | Master |       1 |      3 GB |     -     |
| YARN      | ATS       | Master |       1 |      1 GB |     -     |
| YARN      | NM        |  Slave |       1 |      1 GB | 1x 500 GB |
| RM        | JHS       | Master |       1 |      1 GB |     -     |
| Spark     | Spark HS  | Master |       1 |      2 GB |     -     |
|           |           |        |         |           |           |

Having two racks, we will have one master in each.

> The ATS, JHS and Spark HS should be able to be recreated in the other master in case of failure, meaning that we should take their footprint into consideration even when not installed/running.

With that in mind we should take into consideration for the Master, at this moment:

| Service   | Component | Type   | # Cores | Heap Size | Disk      |
|-----------|-----------|--------|--------:|----------:|:----------|
| ZooKeeper | ZK        | Master |       1 |      1 GB | 1x 100 GB |
| HDFS      | NN        | Master |       4 |      1 GB | 1x 500 GB |
| HDFS      | ZKFC      |  Slave |       1 |      1 GB |     -     |
| YARN      | RM        | Master |       1 |      3 GB |     -     |
| YARN      | ATS       | Master |       1 |      1 GB |     -     |
| RM        | JHS       | Master |       1 |      1 GB |     -     |
| Spark     | Spark HS  | Master |       1 |      2 GB |     -     |
|           |           |        |  **10** |  **10 GB**|           |

As for the slaves we have:

| Service   | Component | Type   | # Cores | Heap Size | Disk      |
|-----------|-----------|--------|--------:|----------:|:----------|
| HDFS      | JN        |  Slave |       1 |      1 GB |     -     |
| HDFS      | DN        |  Slave |       1 |      4 GB |     -     |
| YARN      | NM        |  Slave |       1 |      1 GB | 1x 500 GB |
|           |           |        |   **3** |  **6 GB** |           |

> These are the baselines and in terms of RAM we only discussed Heap, we should account for the OS, expect growth and also leave a margin for ad-hoc requirements. In the case of Slaves we need yet to account for HDFS Storage and YARN Computation. To be addressed in Hardware Specification.

### Apache Kafka

[Apache Kafka](https://kafka.apache.org) is a distributed streaming platform with three key capabilities:

- Publish and subscribe to streams of records, similar to a message queue or enterprise messaging system.
- Store streams of records in a fault-tolerant durable way.
- Process streams of records as they occur.

For our case we will need the next stack:

![Current Stack](./img/kafka-diagram.png)

In terms of Producers and Consumers [Spark](https://spark.apache.org) was already discussed and will use [Hadoop](https://hadoop.apache.org) as the management, which we already seen as well.

Kafka Consumers will need nodes to run, if no Edge nodes are present we will try to run them with the given architecture.

[InfluxDB](https://www.influxdata.com/products/influxdb-overview/) will be discussed after.

A [ZooKeeper](https://zookeeper.apache.org) ensemble is also required, we already draft this for the Hadoop, the same applies.

Hence we are left with:

- Kafka Brokers
- Schema Registry

#### Kafka Schema Registry

[Confluent Schema Registry](https://docs.confluent.io/current/schema-registry/index.html) provides a RESTful interface for storing and retrieving your Schemas.

Schema Registry uses Kafka as a commit log to store all registered schemas durably, and maintains a few in-memory indices to make schema lookups faster.

Typically not CPU- bound and a 1 GB heap size is a good default.

| Service   | Component       | Type   | # Cores | Heap Size | Disk      |
|-----------|-----------------|--------|--------:|----------:|:----------|
| Kafka     | Schema Registry |  Slave |       1 |      1 GB |     -     |
|           |                 |        |         |           |           |

Following the best practices, Schema Registry lives outside of and separately from your Kafka brokers. At this time and taking the considerations of the workload, as long we setup more than one Schema Registry I suggest we co-locate them.

#### Kafka Brokers

In order to run in production a minimum of 3 Brokers is suggest, and my recommendations in terms of hardware for this environment is:

| Service   | Component       | Type   | # Cores | Heap Size | Disk      |
|-----------|-----------------|--------|--------:|----------:|:----------|
| Kafka     | Broker          | Master |      12 |      4 GB | 1x 500 GB |
|           |                 |        |         |           |           |

### RabbitMQ

[RabbitMQ](https://www.rabbitmq.com) is an extensible message queuing solution. It is a message broker that understands Advanced Message Queuing Protocol ([AMQP](https://www.amqp.org)), but is also able to be used with other popular messaging solutions like [MQTT](http://mqtt.org). It is highly available, fault tolerant and scalable.

![RabbitMQ](./img/rabbitmq_logo_strap.png)

We didn't have too much detail in requirements for this piece, but for what we saw it can be, although not perfect, install alongside the Kafka Brokers for now.

We will use the figures presented in [RabbitMQ Resources](#RabbitMQ-Resources)

| Service   | Component       | Type   | # Cores | Heap Size |
|-----------|-----------------|--------|--------:|----------:|
| RabbitMQ  | Broker          | Master |       4 |      8 GB |
|           |                 |        |         |           |

### InlfuxDB

[InfluxDB](https://www.influxdata.com/products/influxdb-overview/) is a high-performance data store written specifically for time series data.

![InfluxDB](./img/Influxdb_logo.svg.png)

For the use case presented we can start by using a simple single-instance, like we talked in the [In-Memory](#In-Memory)  section, in one of the masters and evaluate.

| Service   | Component       | Type   | # Cores | Heap Size |
|-----------|-----------------|--------|--------:|----------:|
| InfluxDB  | Server          | Master |       4 |     20 GB |
|           |                 |        |         |           |

> The use of dedicated of two SSD volumes is recommended when running in production, but due to the nature of the use case I would not recommend it at this time.

## Requirements

### System Requirements

Any major Linux distribution should be consider when running in production any of the tools mentioned in this stack.

For security and uniform across all hosts we recommend the latest released version of JDK 1.8.

Extra packages may be required and are dependent on the OS choice. However they are well documented and easily available.

Drives should be partitioned using XFS, or ext4.

We should take into consideration that the OS itself need some resources.

> Let's have as a base reservation of 1 core and 8 GB for the OS, we are not taking into account Page Cache, which has a big impact in systems like Hadoop and Kafka.

### Hardware Specifications

The design presented takes into consideration the availability of only two racks on the site.

This has a impact, especially while drafting the Masters.

The disks requirements that will follow are for applications only, and segregated from the OS.

Like we talked in the meetings we can choose to go with the BOSS controller card with 2 M.2 Sticks, having RAID 1, instead of the 240 GB we can opt for the 120 GB.

> Keep in mind that although redundancy is met over the disks, the controller card is a SPOF.

On top of that one can even consider having a dedicated disk(s) to filesystem of the nature of `/tmp` and `/var` (especially `/var/log`).

### Master Requirements

There are two types of master in the architecture:

- Hadoop - which will hold the master services for the Hadoop Ecosystem
- Kafka - which will hold the Kafka Brokers and at this time RabbitMQ as well

#### Hadoop Master Sizing

Like we already seen in [Hadoop Layout](#Hadoop-Layout) we should take into account:

| Service   | Component | Type   | # Cores | Heap Size | Disk      |
|-----------|-----------|--------|--------:|----------:|:----------|
| ZooKeeper | ZK        | Master |       1 |      1 GB | 1x 100 GB |
| HDFS      | NN        | Master |       4 |      1 GB | 1x 500 GB |
| HDFS      | ZKFC      |  Slave |       1 |      1 GB |     -     |
| YARN      | RM        | Master |       1 |      3 GB |     -     |
| YARN      | ATS       | Master |       1 |      1 GB |     -     |
| RM        | JHS       | Master |       1 |      1 GB |     -     |
| Spark     | Spark HS  | Master |       1 |      2 GB |     -     |
|           |           |        |  **10** |  **10 GB**|           |

Adding the reservation for OS we have 11 Cores and 18 GB of RAM. Keep in mind that:

- we are not estimating Page Cache for the NN
- some heap sizes may be increased over time
- [InfluxDB](#InfluxDB) can be put in this hosts
- in case of failure of one master we may need to run every component on the same host, minus the standby components

#### Kafka Master Sizing

Like we already seen we should take into account:

| Service   | Component       | Type   | # Cores | Heap Size | Disk      |
|-----------|-----------------|--------|--------:|----------:|:----------|
| ZooKeeper | ZK              | Master |       1 |      1 GB | 1x 100 GB |
| Kafka     | Schema Registry |  Slave |       1 |      1 GB |     -     |
| Kafka     | Broker          | Master |      12 |      4 GB | 1x 500 GB |
|           |                 |        |         |           |           |

Adding the reservation for OS discussed in [System Requirements](#System-Requirements) we have 15 Cores and 14 GB of RAM. Keep in mind that:

- we are not estimating Page Cache for the Kafka Broker
- some heap sizes may be increased over time
- [RabbitMQ](#RabbitMQ) and [InfluxDB](#InfluxDB) can be put in this hosts
- in case of failure of one hadoop master we may need more components on the same host, minus the standby components

### Master Hardware recommendations

With the figures above the recommendation is to have uniform Masters in order to address some failure in the future, and instead of 5 we would recommend to have 6 masters.

Let's see the layout for 5 Masters:

![5 Master Layout](./img/5-Master-layout.png)

> Consider adding an extra disk for use with extra software, like InfluxDB.

Having that extra master not only enable us to spread more components to another node, we could put ATS, JHS, SHS and InlfuxDB running on it, but actual can serve us as a gateway server, and can be use to run the consumers and producers on it, for example.

Also take into consideration that a loss of Rack 1 can happen, and we will be left without a majority for ZooKeeper.

The loss of two Kafka Brokers would also halt the environment.

> We need automate mechanisms to address these scenarios, although extreme scenarios.

### Slave Requirements

There are only one type of slave in the architecture:

- Hadoop - which will hold the HDFS data and YARN computation capabilities

Like in the case of Master, having two racks only also affects the slave architecture. With that in mind the minimum number of slaves to consider are 4, and we recommend 6.

#### Slave Storage Sizing

In terms of dedicated storage for the slaves we already pointed out the need for a 500 GB disk for the Local and Log directories of the NM. On top of that we need the actual HDFS storage.

The estimate size for our system is to reach 30 TB in 5 years, 6 TB per year, meaning that for a replication factor of 3 we would need 90 TB full capacity.

Having two racks means that we can in-fact have one copy of the data per rack, and the third one will be split between the racks. Meaning, also, that if we loose one rack we can only guarantee one full copy which increase the risk of loosing data.

> This only applies when looking at the five year time, in fact we can guarantee at least two copies as long as we don't reach something like 22 TB.

Hence, we can just go into the simple 45 TB per rack, giving the total to 90 TB, or we can add an extra resiliency and put 60 TB, giving a total of 120 TB, per rack in order to be able to have 2 full copies of the data if necessary.

Some pointers per data node:

- minimum of 4 disks
- maximum of 24 disks
- disks shouldn't be larger than 8 TB

Hence, we can have something like this per node:

| # DNs | 90 TB | 120 TB |
|-------|------:|-------:|
|     4 | 23 TB |  30 TB |
|     6 | 15 TB |  20 TB |
|       |       |        |

This translates into:

| # DNs | 90 TB      | 120 TB     |
|-------|-----------:|-----------:|
|     4 | 10x 2.4 TB | 13x 2.4 TB |
|     6 |  7x 2.4 TB |  9x 2.4 TB |
|       |            |            |

> Having ultra-dense DNs will affect recovery times in the event of machine or rack failure. Also take into consideration the number of slots available.

#### Slave Computing Sizing

In terms of daemons running on the slave nodes we already seen that:

| Service   | Component | Type   | # Cores | Heap Size | Disk      |
|-----------|-----------|--------|--------:|----------:|:----------|
| HDFS      | JN        |  Slave |       1 |      1 GB |     -     |
| HDFS      | DN        |  Slave |       1 |      4 GB |     -     |
| YARN      | NM        |  Slave |       1 |      1 GB | 1x 500 GB |
|           |           |        |   **3** |  **6 GB** |           |

Adding the reservation for OS discussed in [System Requirements](#System-Requirements) we have 4 Cores and 14 GB of RAM. Keep in mind that some heap sizes may be increased over time.

In terms of actual computation we've seen in [Application Resources](#Application-Resources) the sum of all the specified workloads we would need:

| Workload    | Driver Cores | Driver Heap Size | # Executors | # Cores | Heap Size | # sessions | Total Cores | Total Heap |
--------------|-------------:|-----------------:|------------:|--------:|----------:|-----------:|------------:|-----------:|
| Heavier Job |            2 |            12 GB |           4 |       4 |     25 GB |          1 |          18 |     112 GB |
| SQL Query   |            1 |             1 GB |           2 |       3 |     15 GB |          1 |           7 |      31 GB |
| Spark RTS   |            1 |             1 GB |           1 |       2 |     10 GB |          4 |          12 |      44 GB |
|             |              |                  |             |         |           |            |          37 |     187 GB |

Hence, if all of them were to be run at the same time we would need: 37 cores and 187 GB RAM.

These numbers represent only the workloads that were already profiled, we need to take into account extra users doing exploratory work and extra processes to be added.

I would consider to, at least, have this capacity per rack, but recommend to have 1.5 or even 2 times that per rack for availability to be used in case of failure.

So in terms of capacity per rack, and rounding the numbers we can go with:

| ratio | # Cores |     RAM |
|-------|--------:|--------:|
|   0.5 |      20 |  100 GB |
|   1.0 |      40 |  200 GB |
|   1.5 |      60 |  300 GB |
|   2.0 |      80 |  400 GB |
|       |         |         |

Giving as the total in the cluster:

| ratio¹ | # Cores |     RAM |
|--------|--------:|--------:|
|   0.5  |      40 |  200 GB |
|   1.0  |      80 |  400 GB |
|   1.5  |     120 |  600 GB |
|   2.0  |     160 |  800 GB |
|        |         |         |
> `¹` - per rack

Hence, in a 4-slave architecture we can go for something like this per node:

| ratio | # Cores |     RAM |
|-------|--------:|--------:|
|   0.5 |      10 |   50 GB |
|   1.0 |      20 |  100 GB |
|   1.5 |      30 |  150 GB |
|   2.0 |      40 |  200 GB |
|       |         |         |

> Keep in mind that the work unit to consider is executors (and drive) which can't be break. Picking up the Heavier Job a single executor takes 25 GB, which for a 0.5 ratio would exaust 2 worker nodes, and use an extra one for the driver, tolerating only a failure of 1, and leaving a few room to run other jobs.

Now, for a 6-slave we would have:

| ratio | # Cores |     RAM |
|-------|--------:|--------:|
|   0.5 |       7 |   34 GB |
|   1.0 |      14 |   67 GB |
|   1.5 |      20 |  100 GB |
|   2.0 |      27 |  134 GB |
|       |         |         |

 Adding the reserved (R) figures we've seen in the beginning to the table of the computation (C) we have the next total per node in a 4-slave architecture:

| ratio | C Cores | C   RAM | R Cores | R   RAM | # Cores |     RAM |
|-------|--------:|--------:|--------:|--------:|--------:|--------:|
|   0.5 |      10 |   50 GB |       4 |   14 GB |      14 |   54 GB |
|   1.0 |      20 |  100 GB |       4 |   14 GB |      24 |  114 GB |
|   1.5 |      30 |  150 GB |       4 |   14 GB |      34 |  164 GB |
|   2.0 |      40 |  200 GB |       4 |   14 GB |      44 |  214 GB |
|       |         |         |         |         |         |         |

For a 6-slave architecture:

| ratio | C Cores | C   RAM | R Cores | R   RAM | # Cores |     RAM |
|-------|--------:|--------:|--------:|--------:|--------:|--------:|
|   0.5 |       7 |   34 GB |       4 |   14 GB |      11 |   48 GB |
|   1.0 |      14 |   67 GB |       4 |   14 GB |      18 |   81 GB |
|   1.5 |      20 |  100 GB |       4 |   14 GB |      24 |  114 GB |
|   2.0 |      27 |  134 GB |       4 |   14 GB |      31 |  148 GB |
|       |         |         |         |         |         |         |

### Slave Hardware recommendations

With the figures above the recommendation is to have a minimum of 4-slaves, but recommend 6-slaves, with the next specs:

| n-slave | cores | RAM    | data disks | hdfs disks |
|--------:|------:|-------:|-----------:|-----------:|
| 4       | 2x 18 | 192 GB |1x 500 GB   | 13x 2.4 TB |
| 6       | 2x 18 | 144 GB |1x 500 GB   |  9x 2.4 TB |
|         |       |        |            |            |
